import { observable, action, computed } from "mobx";


interface IPizza {
  toppings: string[];
}

export class PizzaStore implements IPizza {
  @observable toppings: string[] =  ["pineapple", "cheese"];
  @observable tooManyToppings: boolean = false;
  // when(() => this.numberOfToppings > 5, this.setToppingsLimit);
  @action 
  setToppingsLimit() {
    this.tooManyToppings = true;
  }

  @action
  addTopping(topping: string) {
    this.toppings.push(topping);
  }

  @action
  removeTopping(topping: string) {
    this.toppings.splice(this.toppings.indexOf(topping), 1);
  }

  // NOTE: computedFn (also in mobx) allows you to take 1 or more args to compute value
  @computed
  get numberOfToppings() {
    if(this.toppings.length < 0) {
      throw new Error('Negative toppings');
    }
    return `This pizza has ${this.toppings.length} toppings`;
  }
}
