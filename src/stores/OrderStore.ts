import { observable, computed, action, runInAction } from "mobx";

export class OrderStore {
  @observable orderDate: Date = new Date();
  @observable firstName: string = "Sampada";
  @observable lastName: string = "Sakpal";
  @observable sent: boolean = false;

  @computed
  get orderName() {
    return `${this.firstName} ${this.lastName}`;
  }

  @action
  changeFirstName(newFirstName: string) {
    this.firstName = newFirstName;
  }

  @action
  changeLastName(newLastName: string) {
    this.lastName = newLastName;
  }

  // asynchronous action
  @action
  sendOrder() {
    setTimeout(() => runInAction('sent order after timeout', () => {this.sent = true}), 5000);
  }
}
