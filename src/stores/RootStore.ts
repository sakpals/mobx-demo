import { PizzaStore } from "./PizzaStore";
import { OrderStore } from "./OrderStore";

export interface IRootStore {
  pizza: PizzaStore;
  orderDetails: OrderStore;
}

export class RootStore implements IRootStore {
  pizza: PizzaStore;
  orderDetails: OrderStore;
  constructor() {
    this.pizza = new PizzaStore();
    this.orderDetails = new OrderStore();
  }
}
