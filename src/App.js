import React from "react";
import Pizza from "./components/pizza";
import OrderDetails from "./components/order-details";
import DevTools from "mobx-react-devtools";

function App() {
  return (
    <div style={{ textAlign: "center" }}>
      <DevTools />
      <Pizza />
      <OrderDetails />
    </div>
  );
}
export default App;
