import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import { Provider } from "mobx-react";
import { RootStore } from "./stores/RootStore";

it("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(
    <Provider rootStore={new RootStore()}>
      <App />
    </Provider>,
    div
  );
  ReactDOM.unmountComponentAtNode(div);
});
