
import { PizzaStore } from "../stores/PizzaStore";

it('create Pizza ', () => {
    const pizza = new PizzaStore();
    expect(pizza.numberOfToppings).toEqual(2);
})

it('add 5 toppings for total of 7 (2 initial toppings exist)', () => {
    const pizza = new PizzaStore();
    pizza.addTopping('mushrooms');
    pizza.addTopping('onions');
    pizza.addTopping('basil');
    pizza.addTopping('extra cheese');
    pizza.addTopping('hot sauce');
    expect(pizza.numberOfToppings).toEqual(7)
    expect(pizza.tooManyToppings).toBe(false) // since not implemented yet
    
})