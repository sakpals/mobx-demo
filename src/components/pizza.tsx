import React from "react";
import { inject, observer } from "mobx-react";
import { IRootStore } from "../stores/RootStore";


interface Props {
  rootStore: IRootStore;
}

@inject("rootStore") // like connect() in redux
@observer
class Pizza extends React.Component<Props> {

  addTopping = () => {
    const { pizza } = this.props.rootStore;
    const topping = (document.getElementById('topping') as HTMLInputElement).value;
    topping && pizza.addTopping(topping);
  }

  render() {
    const { pizza } = this.props.rootStore;
   
    return (
      <div>
        <h1>PIZZA</h1>
        <p>{pizza.numberOfToppings}</p>
        Add topping: <input id='topping' type="text" defaultValue=""/>
        <button onClick={this.addTopping} disabled={pizza.tooManyToppings}>Add</button>
        {pizza.toppings.map((t, i) => (
          <div key={i}>
            <button onClick={() => pizza.removeTopping(t)}>Remove</button>
            <p>{t}</p>
          </div>
        ))}
      </div>
    );
        }
}


export default Pizza;
