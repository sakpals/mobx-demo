import React from "react";
import { inject, observer } from "mobx-react";
import { IRootStore } from "../stores/RootStore";

interface Props {
  rootStore: IRootStore;
}

@inject("rootStore")
@observer
class OrderDetails extends React.Component<Props> {

  handleChange = () => {
    const { orderDetails } = this.props.rootStore;
    orderDetails.changeLastName('S');
  }

  sendOrder = () => {
    const { orderDetails } = this.props.rootStore;
    orderDetails.sendOrder();
  }
  render() {
    const { orderDetails } = this.props.rootStore;
    // try rendering just orderDetails.firstName
    // NOTE: if this component was more complex, you could make smaller components that could react independently to changes to minimise re-renders
    return <div>
    {`Order for: ${orderDetails.orderName}`}<br/>
    <button onClick={this.handleChange}>Change last name to 'S'</button>
    {!orderDetails.sent && <button onClick={this.sendOrder}>Submit Order</button>}
    </div>;

    // TODO: add asynchronous action (i.e. submitting order), show use of runInAction and async/await
  }
}

export default OrderDetails;
